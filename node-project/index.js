const express = require("express");
const app = express();
const fs = require("fs");
const alert = require("alert");
const bcrypt = require("bcryptjs");


const filename = "./src/db.json";
const port = 3000;

//middleware
app.use(express.static('src'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.listen(port);


// Default page 
app.get("/", (req, res) => {
    res.sendFile(__dirname+"/src/login.html");
})


// post request after user is registered and then gets redirected to login
app.post("/", async (req, res) => {
    let checkUser = true

    let user = req.body;
    let {name, email, dob, password} = req.body;

    let hashPassword = await bcrypt.hash(password, 10);
    let hashEmail = await bcrypt.hash(email, 10);

    let data = fs.readFileSync(filename);
    let db = JSON.parse(data);

    db.users.forEach(user => {
        if(user.name === name || user.email === email){
            checkUser = false;
            alert("User Already Exists");
            res.sendFile(__dirname+"/src/register.html");
        }
    })
    if(checkUser){
        user.password = hashPassword;
        user.email = hashEmail;
        db.users.push(user);
        fs.writeFile(filename, JSON.stringify(db, null, "\t"), err => {
            if(err) console.log(err);
            res.sendFile(__dirname+"/src/login.html");
        })
    }

})

// Registration page
app.get("/register", (req, res) => {
    res.sendFile(__dirname+"/src/register.html");
})


// Home page
app.post("/home",(req, res) => {
    let checkUser = false;
    
   let {name, password} = req.body;

    let data = fs.readFileSync(filename);
    let db = JSON.parse(data);


    db.users.forEach( async (user) => {
        if(name === user.name){
            let validPass = null; 
            validPass = await bcrypt.compare(password, user.password);
            if(validPass){
                checkUser = true;
                return
            }
            if(password === user.password){
                checkUser = true;
                return
            }
        }
        checkUser = false;
    })

    // db.users.forEach(user => {
    //     if(name === user.name){
    //         if(password === user.password){
    //             checkUser = true;
    //             return
    //         }
    //         checkUser = false;
    //         return
    //     }
    // })
    
    if(checkUser){
        res.sendFile(__dirname+"/src/homepage.html");
    }
    res.end();

})