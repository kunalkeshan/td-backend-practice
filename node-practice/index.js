// const http = require("http");
// const url = require("url");
// const fs = require("fs");

// function OnRequest(req, res){
//     res.writeHead(200, {"content-type": "text/html"});
//     let link = url.parse(req.url, true);
//     if(link.pathname === "/"){
//         fs.readFile("./index.html",(err, data)=>{
//             if(err) res.end("error occured");
//             res.end(data)
//         });
//     }
//     else if(link.pathname === "/add" && req.method === "GET"){
//         res.end("this is get");
//     }
//     else if(link.pathname === "/add" && req.method === "POST"){
//         res.end("this is post");
//     }
// }



// let server = http.createServer(OnRequest)

// server.listen(3000);

const express = require("express");
const app = express();
const fs = require("fs");
const filename = "./db.json";

app.use(express.urlencoded({extended: false}));
app.use(express.json());

app.get("/", (req, res)=>{
    res.sendFile(__dirname + "/index.html");
})

app.post("/add", (req, res)=>{
    let user = req.body;
    let data = fs.readFileSync(filename);
    let db = JSON.parse(data);
    db.users.push(user);
    fs.writeFile(filename, JSON.stringify(db, null, "\t"), (err) => {
        if(err) console.log(err);
        res.send(`user added <a href="/">Go Back</a>`);
    });
})

app.get("/view", (req, res) => {
    let data = fs.readFileSync(filename);
    let db = JSON.parse(data);
    res.writeHead(200, {"Content-type": "text/html"});
    res.write("<table> <tr> <th>Name</th> <th>rno</th> <th>Email</th> </tr>")
    db.users.forEach(user => {
        res.write(`<tr> <td>${user.name}</td><td>${user.rno}</td><td>${user.email}</td></tr>`);
    })
    res.write(`</table> <a href="/">Go Back</a>`);
    res.end()
})

app.post("/update", (req, res) => {
    let data = fs.readFileSync(filename);
    let db = JSON.parse(data);
    let rno = req.body.rno;
    let newEmail = req.body.email;
    db.users.forEach(user => {
        if(user.rno === rno) user.email = newEmail;
    })
    fs.writeFile(filename, JSON.stringify(db, null, "\t"), (err) => {
        if(err) console.log(err);
        res.send(`user email updated <a href="/">Go Back</a>`);
    });
})

app.post("/delete", (req, res) => {
    let data = fs.readFileSync(filename);
    let db = JSON.parse(data);
    let rno = req.body.rno;
    let updatedDb = {users: []};
    updatedDb.users = db.users.filter(user => {
        return !user.rno.includes(rno);
    })
    fs.writeFile(filename, JSON.stringify(updatedDb, null, "\t"), (err) => {
        if(err) console.log(err);
        res.send(`user deleted <a href="/">Go back</a>`)
    });
})

app.listen(3000)